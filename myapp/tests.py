from django.test import TestCase, Client
from django.urls import resolve
from .views import home, signup, loginSuccess

class Story10UnitTest(TestCase):

	def test_story_10_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed('home.html')

	def test_story_10_using_home_func(self):
		found = resolve('/')
		self.assertEqual(found.func, home)

	def test_story_10_signup_function(self):
		found = resolve('/signup/')
		self.assertEqual(found.func, signup)

	def test_loginSuccess_function(self):
		found = resolve('/loginSuccessful/')
		self.assertEqual(found.func, loginSuccess)
